package xyz.stankovic.hsr;

import org.junit.Test;
import xyz.stankovic.hsr.Exceptions.CircularModulePrerequisiteSearchException;
import xyz.stankovic.hsr.logic.StudyPlanCreator;
import xyz.stankovic.hsr.models.ModuleModel;
import xyz.stankovic.hsr.models.SemesterModel;
import xyz.stankovic.hsr.models.StudyPlanModel;
import xyz.stankovic.hsr.utils.ModuleImporter;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by nikola on 01.11.15.
 */
public class ModuleTest {

    @Test
    public void testIsEquals() throws Exception {
        ModuleModel moduleDb1_ONE;
        ModuleModel moduleDb1_TWO;

        String abbreviationDb1 = "db1";

        String abbreviationOO = "OO";
        ModuleModel moduuleOO = new ModuleModel(abbreviationOO);
        HashSet<ModuleModel> prerequisite = new HashSet<>();
        prerequisite.add(moduuleOO);

        moduleDb1_ONE = new ModuleModel(abbreviationDb1, prerequisite);
        moduleDb1_TWO = new ModuleModel(abbreviationDb1, prerequisite);

        boolean expected = true;
        boolean actual = moduleDb1_ONE.equals(moduleDb1_TWO);

        assertEquals("Equals implementation is not correct!", expected, actual);
    }

    @Test(expected = CircularModulePrerequisiteSearchException.class)
    public void testCircularModulePrerequisiteSearchException() throws CircularModulePrerequisiteSearchException {
        HashSet<ModuleModel> modules = ModuleImporter.importModules("CircularModuleCatalogue.txt");
        new StudyPlanCreator(modules);
    }

    @Test
    public void testCreateStudyplan() {
        HashSet<ModuleModel> modules = ModuleImporter.importModules("SmallCatalogue.txt");
        StudyPlanModel expectedStudyPlan = createFixedStudyPlan(modules);

        try {
            StudyPlanCreator studyPlanCreator = new StudyPlanCreator(modules);
            StudyPlanModel actualStudyPlan = studyPlanCreator.createStudyPlan();
            assertEquals(expectedStudyPlan, actualStudyPlan);

        } catch (CircularModulePrerequisiteSearchException e) {
            fail();
        }
    }

    private StudyPlanModel createFixedStudyPlan(HashSet<ModuleModel> modules) {

        // module for the first semester:
        ModuleModel moduleOO = new ModuleModel("OO");

        // module for the second semester:
        HashSet<ModuleModel> prerequisites = new HashSet<>();
        prerequisites.add(moduleOO);

        ModuleModel moduleDB1 = new ModuleModel("DB1", prerequisites);

        // module for the third semester:
        prerequisites.add(moduleDB1);
        ModuleModel moduleDB2 = new ModuleModel("DB2", prerequisites);

        // creating study plan
        StudyPlanModel studyPlan = new StudyPlanModel();

        SemesterModel firstSemester = new SemesterModel();
        firstSemester.addModule(moduleOO);

        SemesterModel secondSemester = new SemesterModel();
        secondSemester.addModule(moduleDB1);

        SemesterModel thirdSemester = new SemesterModel();
        thirdSemester.addModule(moduleDB2);

        studyPlan.getSemesters().add(firstSemester);
        studyPlan.getSemesters().add(secondSemester);
        studyPlan.getSemesters().add(thirdSemester);

        return studyPlan;
    }
}
