package xyz.stankovic.hsr.logic;

import xyz.stankovic.hsr.Exceptions.CircularModulePrerequisiteSearchException;
import xyz.stankovic.hsr.models.ModuleModel;
import xyz.stankovic.hsr.models.SemesterModel;
import xyz.stankovic.hsr.models.StudyPlanModel;

import java.util.HashSet;

/**
 * Created by nikola on 04.11.15.
 */
public class StudyPlanCreator {

    private StudyPlanModel studyPlan;
    private HashSet<ModuleModel> modules;

    public StudyPlanCreator(HashSet<ModuleModel> modules) throws CircularModulePrerequisiteSearchException {
        this.studyPlan = new StudyPlanModel();
        this.modules = modules;

        if (!moduleWithNoPrerequisiteExists()) {
            CircularModulePrerequisiteSearchException exception =
                    new CircularModulePrerequisiteSearchException("Es existiert kein Modul ohne vorausgesetzte Module!");
            throw exception;
        }
    }

    private boolean moduleWithNoPrerequisiteExists() {
        for (ModuleModel module : modules) {
            if (module.getPrerequisites().size() == 0) {
                return true;
            }
        }

        return false;
    }

    public StudyPlanModel createStudyPlan() {

        for (int counter = 0; modules.size() > 0; counter++) {

            SemesterModel actualSemester = new SemesterModel();

            for (ModuleModel module : modules) {
                if (module.getPrerequisites().size() == 0) {
                    actualSemester.addModule(module);
                }
            }

            studyPlan.addSemester(actualSemester);
            removeUsedModules(counter);
        }

        return studyPlan;
    }

    private void removeUsedModules(int semesterIndex) {
        HashSet<ModuleModel> modulesInSemester = studyPlan.getSemesters().get(semesterIndex).getModules();

        modules.removeAll(modulesInSemester);

        for (ModuleModel module : modules) {
            module.getPrerequisites().removeAll(modulesInSemester);
        }
    }
}
