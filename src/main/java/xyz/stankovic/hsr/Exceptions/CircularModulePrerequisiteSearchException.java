package xyz.stankovic.hsr.Exceptions;

/**
 * Created by nikola on 05.11.15.
 */
public class CircularModulePrerequisiteSearchException extends Exception {
    public CircularModulePrerequisiteSearchException(String message) {
        super(message);
    }
}
