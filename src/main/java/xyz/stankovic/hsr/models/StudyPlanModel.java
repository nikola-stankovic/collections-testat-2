package xyz.stankovic.hsr.models;

import lombok.Data;

import java.util.ArrayList;

@Data
public class StudyPlanModel {

    private ArrayList<SemesterModel> semesters = new ArrayList<>();

    public void addSemester(SemesterModel semester) {
        semesters.add(semester);
    }

    public void removeSemester(SemesterModel semester) {
        semesters.remove(semester);
    }

    @Override
    public String toString() {
        String string = new String();

        for (int counter = 0; counter < semesters.size(); counter++) {
            string += "\n" + (counter + 1) + ". Semester: " + semesters.get(counter).toString();
        }

        return string;
    }
}


