package xyz.stankovic.hsr.models;

import lombok.Data;

import java.util.HashSet;

@Data
public class SemesterModel {

    private HashSet<ModuleModel> modules = new HashSet<>();

    public void addModule(ModuleModel module) {
        modules.add(module);
    }

    public void removeModule(ModuleModel module) {
        modules.remove(module);
    }

    @Override
    public String toString() {
        String string = new String();

        for (ModuleModel module : modules) {
            string += module.toString();
        }

        return string;
    }
}