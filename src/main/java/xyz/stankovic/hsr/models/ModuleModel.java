package xyz.stankovic.hsr.models;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;

@Getter
@Setter
public class ModuleModel {

    private String abbreviation;
    private HashSet<ModuleModel> prerequisites = new HashSet<>();

    public ModuleModel(String abbreviation, HashSet<ModuleModel> prerequisite) {
        this.abbreviation = abbreviation;
        this.prerequisites = prerequisite;
    }

    public ModuleModel(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public void addPrerequisite(ModuleModel prerequisite) {
        prerequisites.add(prerequisite);
    }

    public void removePrerequisite(ModuleModel prerequisite) {
        prerequisites.remove(prerequisite);
    }

    @Override
    public String toString() {
        return abbreviation + " ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ModuleModel that = (ModuleModel) o;

        return abbreviation.equals(that.abbreviation);

    }

    @Override
    public int hashCode() {
        return abbreviation.hashCode();
    }
}
