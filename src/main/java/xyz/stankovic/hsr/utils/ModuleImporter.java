package xyz.stankovic.hsr.utils;

import xyz.stankovic.hsr.logic.CatalogueReader;
import xyz.stankovic.hsr.models.ModuleModel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by nikola on 01.11.15.
 */
public class ModuleImporter {

    public static HashSet<ModuleModel> importModules(String fileName) {

        HashSet<ModuleModel> modules = new HashSet<>();

        // TODO: code it OS independent
        String filePath = System.getProperty("user.dir") + "/res/" + fileName;

        try (CatalogueReader reader = new CatalogueReader(filePath)) {
            String[] names;

            while ((names = reader.readNexteLine()) != null) {
                String moduleAbbreviation = names[0];
                HashSet<ModuleModel> prerequisite = new HashSet<>();

                for (int i = 1; i < names.length; i++) {
                    String abbreviation = names[i];

                    ModuleModel module = moduleExists(abbreviation, modules);

                    if (module == null) {
                        module = new ModuleModel(abbreviation);
                    }

                    prerequisite.add(module);
                }

                ModuleModel module = new ModuleModel(moduleAbbreviation, prerequisite);
                modules.add(module);
            }
        } catch (FileNotFoundException e) {
            // TODO: Implement exception handling!
            System.out.println("FAIL: FileNotFoundException");

        } catch (IOException e) {
            // TODO: Implement exception handling!
            System.out.println("FAIL: IOException");
        } catch (Exception e) {
            // TODO: Implement exception handling!
            System.out.println("FAIL: Exception");
        }

        return modules;
    }

    private static ModuleModel moduleExists(String abbreviation, HashSet<ModuleModel> modules) {

        if (abbreviation.isEmpty()) {
            return null;
        }

        for (ModuleModel module : modules) {
            if (module.getAbbreviation() == abbreviation) {
                return module;
            }
        }

        return null;
    }
}
