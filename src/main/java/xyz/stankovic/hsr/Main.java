package xyz.stankovic.hsr;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import xyz.stankovic.hsr.Exceptions.CircularModulePrerequisiteSearchException;
import xyz.stankovic.hsr.logic.StudyPlanCreator;
import xyz.stankovic.hsr.models.ModuleModel;
import xyz.stankovic.hsr.models.StudyPlanModel;
import xyz.stankovic.hsr.utils.ModuleImporter;

import java.util.HashSet;

public class Main {

    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        BasicConfigurator.configure();

        HashSet<ModuleModel> modules = ModuleImporter.importModules("LargeCatalogue.txt");
        StudyPlanCreator creator;

        long start = System.currentTimeMillis();
        try {
            creator = new StudyPlanCreator(modules);
        } catch (CircularModulePrerequisiteSearchException e) {
            System.out.println(e.getMessage());
            System.out.println("Ein Studienplan kann somit nicht erstellt werden.");
            return;
        }

        StudyPlanModel studyplan = creator.createStudyPlan();
        log.info("Required Time: " + (System.currentTimeMillis() - start) + "ms");

        String output = studyplan.toString();
        log.info(output + "\n");
    }
}
